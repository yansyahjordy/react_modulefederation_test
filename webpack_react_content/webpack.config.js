const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    // publicPath:"http://localhost:3020",
    // path: path.join(__dirname, "/dist"),
    filename:'index.bundle.js'
  },
  devServer: {
    port: 3020,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new ModuleFederationPlugin({
      name: "app2",
      filename: "remoteEntry.js",
      exposes: {
        Content: "./src/Content"
      },
      shared: {
        react: "^18.2.0",
        "react-dom": "^18.2.0",
      },
    }),
  ]
};
